import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ToogleButtonComponent } from './toogle-button.component';

describe('ToogleSidebarComponent', () => {
  let component: ToogleButtonComponent;
  let fixture: ComponentFixture<ToogleButtonComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ToogleButtonComponent]
    });
    fixture = TestBed.createComponent(ToogleButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
