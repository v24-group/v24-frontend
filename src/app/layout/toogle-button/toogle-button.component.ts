import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-toogle-sidebar',
  templateUrl: './toogle-button.component.html',
  styleUrls: ['./toogle-button.component.scss'],
})
export class ToogleButtonComponent implements OnInit{

  showSidebar!: boolean;

  constructor(
    private commonService: CommonService
  ) {
    this.showSidebar = this.commonService.INI_SHOW_SIDEBAR;
  }

  ngOnInit(): void {
  }

  toggle() {
    this.showSidebar = !this.showSidebar;
    this.commonService.toogleSidebar(this.showSidebar);
  }

}
