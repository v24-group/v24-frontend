import { Component, OnInit } from '@angular/core';
import { Observable, of } from 'rxjs';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit{

  toogle$!: Observable<boolean>;

  constructor(
    private commonService: CommonService
  ) {}

  ngOnInit(): void {
    this.toogle$ = this.commonService.toogleSidebar$;
  }

}
