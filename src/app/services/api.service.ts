import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ApiService {

  private API_URL = 'http://localhost:8080';
  private AUTOTEST_ENDPOINT = this.API_URL + '/autotest'

  constructor(private http: HttpClient) {}

  getAutoTest(): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
    });

    return this.http.get<any>(this.AUTOTEST_ENDPOINT, { headers }).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse): Observable<never> {
    let errorMessage = 'Ocurrió un error en la solicitud.';

    if (error.error instanceof ErrorEvent) {
      errorMessage = `Error: ${error.error.message}`;
    } else {
      if (error.status === 0 && error.statusText === 'Unknown Error') {
        // Manejar específicamente el caso en el que no se puede conectar al servidor
        errorMessage = 'No se pudo establecer conexión con el servidor.';
      } else if (error.status === 200 && error.statusText === 'OK') {
        // Manejar específicamente el caso donde el servidor devuelve 200 OK, pero la respuesta no es JSON
        errorMessage = 'La respuesta no es un JSON válido.';
      } else {
        errorMessage = `Código de error: ${error.status}, Mensaje: ${error.message}`;
      }
    }

    console.error(errorMessage);
    return throwError(errorMessage);
  }
}
