import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  INI_SHOW_SIDEBAR = true;
  private toogleSidebarSub: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(this.INI_SHOW_SIDEBAR);
  toogleSidebar$: Observable<boolean> = this.toogleSidebarSub.asObservable();

  constructor() { }

  toogleSidebar(value: boolean) {
    this.toogleSidebarSub.next(value);
  }


}
