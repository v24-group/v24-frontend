import { Component } from '@angular/core';

@Component({
  selector: 'app-card-faq',
  templateUrl: './card-faq.component.html',
  styleUrls: ['./card-faq.component.scss']
})
export class CardFaqComponent {

  entrevistaEntries = [
    { Pregunta: '¿Cuál es tu experiencia previa en desarrollo de software?', Respuesta: 'He trabajado en proyectos de desarrollo web durante los últimos 2 años, centrándome en tecnologías como Angular y Node.js.', mostrarRespuesta: false },
    { Pregunta: '¿Cuáles son tus habilidades técnicas más fuertes?', Respuesta: 'Mis habilidades más fuertes incluyen programación en JavaScript/TypeScript, diseño de API RESTful y experiencia en bases de datos SQL y NoSQL.', mostrarRespuesta: false },
    { Pregunta: '¿Cómo te mantienes actualizado con las tendencias tecnológicas?', Respuesta: 'Leo blogs, participo en comunidades en línea y asisto a conferencias sobre desarrollo de software. También realizo proyectos personales para aplicar nuevas tecnologías.', mostrarRespuesta: false },
    { Pregunta: '¿Has trabajado en proyectos colaborativos anteriormente?', Respuesta: 'Sí, he colaborado en equipos de desarrollo en proyectos anteriores. Utilizamos metodologías ágiles como Scrum para garantizar una comunicación efectiva y entregas a tiempo.', mostrarRespuesta: false },
    { Pregunta: '¿Cuál es tu mayor logro profesional hasta ahora?', Respuesta: 'Uno de mis mayores logros fue liderar un equipo en la creación de una aplicación que resultó en un aumento del 30% en la eficiencia operativa de la empresa.', mostrarRespuesta: false },
    { Pregunta: '¿Qué te motivó a postularte para este bootcamp en particular?', Respuesta: 'La reputación de la universidad, combinada con el enfoque práctico del bootcamp, me motivó a buscar esta oportunidad para acelerar mi crecimiento profesional.', mostrarRespuesta: false },
    { Pregunta: '¿Cuáles son tus expectativas para el bootcamp?', Respuesta: 'Espero adquirir habilidades más avanz'},
  ];

  toggleCard(entry: any): void {
    entry.mostrarRespuesta = !entry.mostrarRespuesta;
  }

}
