import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './admin/pages/inicio/inicio.component';
import { Page1Component } from './admin/pages/page1/page1.component';
import { Page2Component } from './admin/pages/page2/page2.component';
import { Page3Component } from './admin/pages/page3/page3.component';
import { Page4Component } from './admin/pages/page4/page4.component';
import { Page5Component } from './admin/pages/page5/page5.component';
import { NotFoundComponent } from './admin/pages/not-found/not-found.component';
import { FaqComponent } from './admin/pages/faq/faq.component';

const routes: Routes = [
  { path: '', component:  InicioComponent},
  { path: 'home', component:  InicioComponent},
  { path: 'faq', component:  FaqComponent},
  { path: 'page1', component: Page1Component },
  { path: 'page2', component: Page2Component },
  { path: 'page3', component: Page3Component },
  { path: 'page4', component: Page4Component },
  { path: 'page5', component: Page5Component },
  { path: '**', component: NotFoundComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
