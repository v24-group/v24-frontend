import { Component, OnInit } from '@angular/core';
import { ApiService } from './services/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  title = 'v24';

  constructor(
    private apiSErvice: ApiService
  ){}

  ngOnInit(): void {
    this.apiSErvice.getAutoTest().subscribe(
      value => { console.log('Ok autotest', value)},
      error => { console.log('Error autotest', error)}
    );
  }

}
