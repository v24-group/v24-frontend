import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminComponent } from './admin/admin.component';
import { TopbarComponent } from './layout/topbar/topbar.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { ToogleButtonComponent } from './layout/toogle-button/toogle-button.component';

// Angular Material Modules
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { FooterComponent } from './layout/footer/footer.component';
import { InicioComponent } from './admin/pages/inicio/inicio.component';
import { Page1Component } from './admin/pages/page1/page1.component';
import { Page2Component } from './admin/pages/page2/page2.component';
import { Page3Component } from './admin/pages/page3/page3.component';
import { Page4Component } from './admin/pages/page4/page4.component';
import { Page5Component } from './admin/pages/page5/page5.component';
import { NotFoundComponent } from './admin/pages/not-found/not-found.component';
import { HttpClientModule } from '@angular/common/http';
import { CardFaqComponent } from './admin/pages/faq/card-faq/card-faq.component';
import { FaqComponent } from './admin/pages/faq/faq.component';

@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    TopbarComponent,
    SidebarComponent,
    ToogleButtonComponent,
    FooterComponent,
    InicioComponent,
    Page1Component,
    Page2Component,
    Page3Component,
    Page4Component,
    Page5Component,
    NotFoundComponent,
    CardFaqComponent,
    FaqComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonToggleModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
